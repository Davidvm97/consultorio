/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultoriomedico;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author david
 */
@Embeddable
public class CitasPorPacientePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "DocPaciente")
    private int docPaciente;
    @Basic(optional = false)
    @Column(name = "FechaCita")
    @Temporal(TemporalType.DATE)
    private Date fechaCita;

    public CitasPorPacientePK() {
    }

    public CitasPorPacientePK(int docPaciente, Date fechaCita) {
        this.docPaciente = docPaciente;
        this.fechaCita = fechaCita;
    }

    public int getDocPaciente() {
        return docPaciente;
    }

    public void setDocPaciente(int docPaciente) {
        this.docPaciente = docPaciente;
    }

    public Date getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) docPaciente;
        hash += (fechaCita != null ? fechaCita.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CitasPorPacientePK)) {
            return false;
        }
        CitasPorPacientePK other = (CitasPorPacientePK) object;
        if (this.docPaciente != other.docPaciente) {
            return false;
        }
        if ((this.fechaCita == null && other.fechaCita != null) || (this.fechaCita != null && !this.fechaCita.equals(other.fechaCita))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "consultoriomedico.CitasPorPacientePK[ docPaciente=" + docPaciente + ", fechaCita=" + fechaCita + " ]";
    }
    
}
