/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultoriomedico;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david
 */
@Entity
@Table(name = "PagosPorPaciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PagosPorPaciente.findAll", query = "SELECT p FROM PagosPorPaciente p")
    , @NamedQuery(name = "PagosPorPaciente.findByIdDoc", query = "SELECT p FROM PagosPorPaciente p WHERE p.idDoc = :idDoc")
    , @NamedQuery(name = "PagosPorPaciente.findByValor", query = "SELECT p FROM PagosPorPaciente p WHERE p.valor = :valor")})
public class PagosPorPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdDoc")
    private Integer idDoc;
    @Basic(optional = false)
    @Lob
    @Column(name = "MotivoPago")
    private String motivoPago;
    @Basic(optional = false)
    @Column(name = "Valor")
    private int valor;
    private String Estado;
    private Connection cn;

    public PagosPorPaciente() {
        Conexion con=new Conexion();
         cn=con.getConexion();
    }

    public PagosPorPaciente(Integer idDoc) {
        this.idDoc = idDoc;
        Conexion con=new Conexion();
         cn=con.getConexion();
    }

    public PagosPorPaciente(Integer idDoc, String motivoPago, int valor) {
        this.idDoc = idDoc;
        this.motivoPago = motivoPago;
        this.valor = valor;
        Conexion con=new Conexion();
         cn=con.getConexion();
    }

    public void LlenarDatos(DefaultTableModel modelo){
    
        try{
            String sql="select * from PagosPorPaciente";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[5];
                for(int i=0;i<5;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
     public void Agregar (Integer docPaciente, String motivoPago, Integer valor, Integer idPago,String Estado){
        try{
            String sql="execute AgregarPago ?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setString(2, motivoPago);
            cmd.setInt(3, valor);
            cmd.setInt(4, idPago);
            cmd.setString(5, Estado);
            
            cmd.execute();
            cmd.close();
            cn.close();
            
        }catch(Exception ex){
        
            System.out.println(ex.getMessage());
        }
        
    
    }
     
     public void Actualizar (Integer docPaciente, String motivoPago, Integer valor, Integer idPago,String Estado){
        try{
            String sql="execute ActualizarPagos ?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setString(2, motivoPago);
            cmd.setInt(3, valor);
            cmd.setInt(4, idPago);
            cmd.setString(5, Estado);
            
            cmd.execute();
            cmd.close();
            cn.close();
            
        }catch(Exception ex){
        
            System.out.println(ex.getMessage());
            
        }
        
    
    }
     public void Eliminar(Integer docPaciente){
     try{
         String sql="delete PagosPorPaciente from PagosPorPaciente where IdDoc="+docPaciente;
         PreparedStatement cmd=cn.prepareCall(sql);
         cmd.execute();
         cmd.close();
         cn.close();
         
     }catch(Exception ex){
         System.out.println(ex.getMessage());
     
     }
     }
     public void Buscar(DefaultTableModel modelo, String palabra){
    
        try{
            String sql="select * from PagosPorPaciente where IdDoc like'%"+palabra+"%'";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[5];
                for(int i=0;i<5;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
     public void BuscarPendientes(DefaultTableModel modelo){
    
        try{
            String sql="select * from PagosPorPaciente where PagosPorPaciente.Estado='Pendiente'";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[5];
                for(int i=0;i<5;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
     
     
     
     

    public Integer getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public String getMotivoPago() {
        return motivoPago;
    }

    public void setMotivoPago(String motivoPago) {
        this.motivoPago = motivoPago;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDoc != null ? idDoc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagosPorPaciente)) {
            return false;
        }
        PagosPorPaciente other = (PagosPorPaciente) object;
        if ((this.idDoc == null && other.idDoc != null) || (this.idDoc != null && !this.idDoc.equals(other.idDoc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "consultoriomedico.PagosPorPaciente[ idDoc=" + idDoc + " ]";
    }
    
}
