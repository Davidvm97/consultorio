/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultoriomedico;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david
 */
@Entity
@Table(name = "CitasPorPaciente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CitasPorPaciente.findAll", query = "SELECT c FROM CitasPorPaciente c")
    , @NamedQuery(name = "CitasPorPaciente.findByDocPaciente", query = "SELECT c FROM CitasPorPaciente c WHERE c.citasPorPacientePK.docPaciente = :docPaciente")
    , @NamedQuery(name = "CitasPorPaciente.findByFechaCita", query = "SELECT c FROM CitasPorPaciente c WHERE c.citasPorPacientePK.fechaCita = :fechaCita")})
public class CitasPorPaciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CitasPorPacientePK citasPorPacientePK;
    @Basic(optional = false)
    @Lob
    @Column(name = "Motivo")
    private String motivo;
    @Lob
    @Column(name = "Procedimiento")
    private String procedimiento;
    @Lob
    @Column(name = "Diagnostico")
    private String diagnostico;
    private Connection cn;
    private Date fechaCita;

    public CitasPorPaciente() {
         Conexion con=new Conexion();
         cn=con.getConexion();
        
    }
     public void LlenarDatos(DefaultTableModel modelo){
    
        try{
            String sql="select cpp.DocPaciente, p.NomPaciente, cpp.FechaCita, cpp.Motivo, cpp.Procedimiento, cpp.Diagnostico\n" +
"\n" +
"From CitasPorPaciente cpp inner join Pacientes p on p.DocPaciente=cpp.DocPaciente;";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[6];
                for(int i=0;i<6;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
     public void Agregar (Integer docPaciente, Date fechaCita, String Motivo, String Procedimiento, String Diagnostico){
        try{
            String sql="execute AgregarCita ?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setDate(2,  fechaCita);
            cmd.setString(3, Motivo);
            cmd.setString(4, Procedimiento);
            cmd.setString(5, Diagnostico);
          
            cmd.execute();
            cmd.close();
            cn.close();
            
        }catch(Exception ex){
        
            System.out.println(ex.getMessage());
        }
        
    
    }
     
     public void Actualizar(Integer docPaciente, Date fechaCita, String Motivo, String Procedimiento, String Diagnostico){
        try{
            String sql="execute ActualizarCitas ?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setDate(2,  fechaCita);
            cmd.setString(3, Motivo);
            cmd.setString(4, Procedimiento);
            cmd.setString(5, Diagnostico);
          
            cmd.execute();
            cmd.close();
            cn.close();
            
        }catch(Exception ex){
        
            System.out.println(ex.getMessage());
        }
    }
     public void Eliminar(Integer docPaciente){
     try{
         String sql="delete CitasPorPaciente from CitasPorPaciente where DocPaciente="+docPaciente;
         PreparedStatement cmd=cn.prepareCall(sql);
         cmd.execute();
         cmd.close();
         cn.close();
         
     }catch(Exception ex){
         System.out.println(ex.getMessage());
     
     }
     }
     public void BuscarPorFechas(DefaultTableModel modelo, Date fechaini, Date fechafin){
    
        try{
            String sql="select cpp.DocPaciente, p.NomPaciente, cpp.FechaCita, cpp.Motivo, cpp.Procedimiento, cpp.Diagnostico\n" +
"\n" +
	"From CitasPorPaciente cpp inner join Pacientes p on p.DocPaciente=cpp.DocPaciente where cpp.FechaCita>='"+fechaini+"' and cpp.FechaCita<='"+fechafin+"'";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[6];
                for(int i=0;i<6;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
    
     
     
     

    public CitasPorPaciente(CitasPorPacientePK citasPorPacientePK) {
        this.citasPorPacientePK = citasPorPacientePK;
    }

    public CitasPorPaciente(CitasPorPacientePK citasPorPacientePK, String motivo) {
        this.citasPorPacientePK = citasPorPacientePK;
        this.motivo = motivo;
    }

    public CitasPorPaciente(int docPaciente, Date fechaCita) {
        this.citasPorPacientePK = new CitasPorPacientePK(docPaciente, fechaCita);
    }

    public CitasPorPacientePK getCitasPorPacientePK() {
        return citasPorPacientePK;
    }

    public void setCitasPorPacientePK(CitasPorPacientePK citasPorPacientePK) {
        this.citasPorPacientePK = citasPorPacientePK;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (citasPorPacientePK != null ? citasPorPacientePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CitasPorPaciente)) {
            return false;
        }
        CitasPorPaciente other = (CitasPorPaciente) object;
        if ((this.citasPorPacientePK == null && other.citasPorPacientePK != null) || (this.citasPorPacientePK != null && !this.citasPorPacientePK.equals(other.citasPorPacientePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "consultoriomedico.CitasPorPaciente[ citasPorPacientePK=" + citasPorPacientePK + " ]";
    }
    
}
