/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultoriomedico;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {
    private Conexion instance;
    Conexion(){}
    
    public static Connection getConexion(){
        Connection con =null;
        
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String connectionUrl="jdbc:sqlserver://LAPTOP-7QKHCA95:1433;"+
                    "databaseName=ConsultorioMedico;user=sa;password=123456;";
            con=DriverManager.getConnection(connectionUrl);
        } catch(SQLException e){
            System.out.println("SQL Exception: "+e.toString());
        
        } catch(ClassNotFoundException cE){
            System.out.println("Class Not Found Exception: "+ cE.toString());
        }
        return con;
    }
    

}
