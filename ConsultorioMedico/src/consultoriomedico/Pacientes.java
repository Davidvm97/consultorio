/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultoriomedico;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.sql.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@Entity
@Table(name = "Pacientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pacientes.findAll", query = "SELECT p FROM Pacientes p")
    , @NamedQuery(name = "Pacientes.findByDocPaciente", query = "SELECT p FROM Pacientes p WHERE p.docPaciente = :docPaciente")
    , @NamedQuery(name = "Pacientes.findByNomPaciente", query = "SELECT p FROM Pacientes p WHERE p.nomPaciente = :nomPaciente")
    , @NamedQuery(name = "Pacientes.findBySexo", query = "SELECT p FROM Pacientes p WHERE p.sexo = :sexo")
    , @NamedQuery(name = "Pacientes.findByCorreo", query = "SELECT p FROM Pacientes p WHERE p.correo = :correo")
    , @NamedQuery(name = "Pacientes.findByCel", query = "SELECT p FROM Pacientes p WHERE p.cel = :cel")
    , @NamedQuery(name = "Pacientes.findByEps", query = "SELECT p FROM Pacientes p WHERE p.eps = :eps")
    , @NamedQuery(name = "Pacientes.findByFechaNac", query = "SELECT p FROM Pacientes p WHERE p.fechaNac = :fechaNac")
    , @NamedQuery(name = "Pacientes.findByTel", query = "SELECT p FROM Pacientes p WHERE p.tel = :tel")})
public class Pacientes implements Serializable {

    @Basic(optional = false)
    @Column(name = "Cel")
    private String cel;
    @Basic(optional = false)
    @Column(name = "Tel")
    private String tel;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pacientes")
   

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DocPaciente")
    private Integer docPaciente;
    @Basic(optional = false)
    @Column(name = "NomPaciente")
    private String nomPaciente;
    @Basic(optional = false)
    @Column(name = "Sexo")
    private String sexo;
    @Basic(optional = false)
    @Column(name = "Correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "EPS")
    private String eps;
    @Basic(optional = false)
    @Column(name = "FechaNac")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;
    @Basic(optional = false)
    @Lob
    @Column(name = "HistClinica")
    private String histClinica;
    private Connection cn;
    
    public Pacientes() {
        Conexion con=new Conexion();
         cn=con.getConexion();
    }

    public Pacientes(Integer docPaciente) {
        Conexion con=new Conexion();
        cn=con.getConexion();
        this.docPaciente = docPaciente;
    }

    public Pacientes(Integer docPaciente, String nomPaciente, String sexo, String correo, String cel, String eps, Date fechaNac, String tel, String histClinica) {
        this.docPaciente = docPaciente;
        this.nomPaciente = nomPaciente;
        this.sexo = sexo;
        this.correo = correo;
        this.cel = cel;
        this.eps = eps;
        this.fechaNac = fechaNac;
        this.tel = tel;
        this.histClinica = histClinica;
        Conexion con=new Conexion();
         cn=con.getConexion();
    }
    
    public void LlenarDatos(DefaultTableModel modelo){
    
        try{
            String sql="select * from Pacientes";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[9];
                for(int i=0;i<9;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }
    public void Agregar (Integer docPaciente, String nomPaciente, String sexo, String correo, String cel, String eps, Date fechaNac, String tel, String histClinica){
        try{
            String sql="execute AgregarPaciente ?,?,?,?,?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setString(2, nomPaciente);
            cmd.setString(3, sexo);
            cmd.setString(4, correo);
            cmd.setString(5, cel);
            cmd.setString(6, eps);
            cmd.setDate(7,fechaNac);
            cmd.setString(8, tel);
            cmd.setString(9, histClinica);
            cmd.execute();
            cmd.close();
            cn.close();
            
        }catch(Exception ex){
        
            System.out.println(ex.getMessage());
        }
        
    
    }
    public void Actualizar(Integer docPaciente, String nomPaciente, String sexo, String correo, String cel, String eps, String tel, String histClinica){
        try{
            String sql="execute ActualizarPacientes ?,?,?,?,?,?,?,?";
            PreparedStatement cmd=cn.prepareCall(sql);
            cmd.setInt(1, docPaciente);
            cmd.setString(2, nomPaciente);
            cmd.setString(3, sexo);
            cmd.setString(4, correo);
            cmd.setString(5, cel);
            cmd.setString(6, eps);
            cmd.setString(7, tel);
            cmd.setString(8, histClinica);
            cmd.execute();
            cmd.close();
            cn.close();
        
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
      public void Eliminar(Integer docPaciente){
     try{
         String sql="delete Pacientes from Pacientes where DocPaciente="+docPaciente;
         PreparedStatement cmd=cn.prepareCall(sql);
         cmd.execute();
         cmd.close();
         cn.close();
         
     }catch(Exception ex){
         System.out.println(ex.getMessage());
     
     }
     }
       public void Buscar(DefaultTableModel modelo, String palabra){
    
        try{
            String sql="select * from Pacientes where NomPaciente like'%"+palabra+"%'";
            CallableStatement cmd=cn.prepareCall(sql);
            
            ResultSet rs= cmd.executeQuery();
            
            while(rs.next()){
                Object [] datos= new Object[9];
                for(int i=0;i<9;i++){
                    datos[i]=rs.getString(i+1);
                }
            modelo.addRow(datos);
            }
        cmd.close();
        cn.close();
        }
        catch(Exception ex){
        System.out.println(ex.getMessage());        
        }
    }

    public Integer getDocPaciente() {
        return docPaciente;
    }

    public void setDocPaciente(Integer docPaciente) {
        this.docPaciente = docPaciente;
    }

    public String getNomPaciente() {
        return nomPaciente;
    }

    public void setNomPaciente(String nomPaciente) {
        this.nomPaciente = nomPaciente;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }


    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }


    public String getHistClinica() {
        return histClinica;
    }

    public void setHistClinica(String histClinica) {
        this.histClinica = histClinica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docPaciente != null ? docPaciente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pacientes)) {
            return false;
        }
        Pacientes other = (Pacientes) object;
        if ((this.docPaciente == null && other.docPaciente != null) || (this.docPaciente != null && !this.docPaciente.equals(other.docPaciente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "consultoriomedico.Pacientes[ docPaciente=" + docPaciente + " ]";
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

  
    
}
